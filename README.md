Uptime Static Protoype
==================

Uptime Frontend Static Prorotype for ARTS Client

-----------

Contains
-----------

1. Bootstrap 3.2.x with Sass
2. jQuery
3. Fontawesome 4.7.0 and Glyphicons
4. Ready-made favicon placeholders
5. HTML with partials including
6. Autoprefixer for CSS
7. Javascript linter
8. CSS/JS copy & Minifier
9. CSS Comb
10. Grunt watch & Notifier

-----------

Prerequisites
-----------

1. [Git](https://git-scm.com/book/en/v2/Getting-Started-Installing-Git).
2. [node.js](http://nodejs.org/) (and NPM)
3. [bower](https://bower.io/) (and NPM)
3. 	grunt-cli: `npm install -g grunt-cli`
4. 	Optional: [livereload browser extension](http://feedback.livereload.com/knowledgebase/articles/86242-how-do-i-install-and-use-the-browser-extensions-)

-----------

How to Start
-----------

1.  To pull this repository, you need to have [Git](https://git-scm.com/book/en/v2/Getting-Started-Installing-Git) installed.
	* If you are new to Git, please consider using software, such as [Sourcetree](https://www.sourcetreeapp.com/).
2.  Follow the instructions under Actions -> Clone on this page to clone the repository to your work station.
3.	Make sure you have Node.js installed. To check if you have it installed correctly, open your terminal/bash/command line program and type `node -v`. If you get a version number, you have Node.js installed. After this, do the same with `npm -v`.
4.	Once you have Node.js and NPM installed, install Bower globally with `npm install -g bower`. Run `bower -v` to see the version. After this, install Grunt by running `npm install -g grunt-cli`.
5.  Once you have Git, node.js and NPM, Bower and Grunt installed, and you have cloned the repository to a local directory, use the  Console to direct yourself to the root of the project (eg. /webclient-frontend).
6.  Run `npm install` - this installs the required Node modules. The installation process should take about a minute.
7. 	Run `bower install` - This installs the required Bower components, which can be used to update the module version.
8.  Run `grunt` - If all is installed correctly, the task runner will work without any problems, and you will see a `Waiting...` at the end of your console.

a.  If you get an error like `Warning: Unable to delete "dist" file (ENOTEMPTY: directory not empty, rmdir 'dist'). Use --force to continue.`, simply delete the /dist folder and run `grunt` again.



-----------

What this does (what Grunt does)
-----------

1. Check the Sass files for missing variables, syntax errors, bad mixins etc.
2. Check the Javascript for missing functions and general lint problems
3. Combine and process the Sass files into one css file
4. Minify said file (as a copy, please see Grunt settings section for more information)
5. Similar process with the Javascript
6. Organize the CSS in the scss files according to Bootstraps CSS semantic guidelines
7. Copy all the "live" files to the /dist folder. Files in this folder need no server to run, but will be replaced on each grunt run.
8. Run a grunt server on 127.0.0.1/3000
9. Run a watch task which auto-reloads the page once changes have been made in the Sass, HTML or JS files
9a. NOTE: If you want to run without watch, run `grunt dist` instead of regular `grunt` in the console.

-----------

Development
-----------
1. **HTML**: Work on the *.html* files and the */partials*
2. **Sass**: Work with the *assets/sass/style.scss and assets/sass* files in abstracts/base/components/layout/pages/themes/vendor. Do not modify the content of the *vendor/bootstrap* folder, unless you replace/update the files from an updated bower component package
3. **JS**: Work with the *assets/js/src/App.js* for your general, App-wide functions
3a. **jQuery plugins**: External plugins can be put in *assets/js/vendor/jquery-plugins*. They will automatically be added to the minififed .js-file which gets included in the project.
4. **Images**: Place your images in the assets/img folder. They will be copied to /dist
5. **Browser requirements**: Set the browser versions in **grunt/configBridge.json**
6. **CSSComb**: Set the rules in *grunt/csscomb.json*

-----------

Theming
-----------
1. **COLORS & VALUES**
-----------------------
+ To change inherent values such as colors and fonts, you only need to change values in the sass/abstracts/variables.scss file. Colors, fonts and simple margins/effects are defined in this file via Sass variables. By changing the value of the variables, this value will be applied on compilation time and will update the value of said variable. For example, if you change the color for the *$brand-primary* variable, it will update the color throughout the application after compilation, and the color you assigned has now replaced the previous one.
	* Please look through the file and see what each variable defines.
	* NOTE: Do not change the variable names themselves. Only add new ones if needed.

2. **IMAGES & LOGOS**
-----------------------
+ To change logos and add images, add/replace the images in the assest/img folder. The image will be automatically copied over to the /dist folder with the grunt task.
	* Add the image to your selected HTML file and location.
	* Please remember that the URL of the image/source file needs to be in accordance to the /dist folder.

3. **FONTS & ICONS**
-----------------------
+ To change the font and icons, add/replace the files in the assest/fonts folder.
	* To change the active fonts, see the *Typograhy* section in the variables.scss file. *$font-family-sans-serif* is the main font of the application.
	* To import the fonts via .scss, see the sass/base/fonts.scss file. Import the files the same way as the Raleway font (each font weight/style needs its own import, though it can and should use the same font-family name defintion). It is always best practice to include different formats of the font as fallbacks for different devices/browsers.

4. **JAVASCRIPT**
-----------------------
+ You can add/disbale JS functions in the assets/js/src/App.js file. Each function needs to be defined under *setup*. Scripts you don't want to run should be commented out under the setup function.
	* Each upper level function needs to be defined by a name. You can define global variables in *setup*.

-----------

Other notes
-----------
+ The relative path set in the Sass files (for example, an URL for a logo) is according to the /dist folder. You can see what gets copied over in the Gruntfile.js