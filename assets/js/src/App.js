/**
 * @author Author Name a.name@insign.ch;
 */
var App = App || {};

/**
 * Namespace which groups all global Options for the Application
 */
App = (function() {

    var xl = 1439;
    var lg = 1199;
    var md = 991;
    var sm = 752;

  return {
    setup: function() {

      // In general: Links that point to # should be ignored
      $("a[href='#']").click(function(e) { e.preventDefault(); });

      // Enable Bootstrap popovers and tooltips
      $("[data-toggle=popover]").popover();
      $("[data-toggle=tooltip]").tooltip();

      // You must add the function here for it to be enabled
      App.footable();
      App.mobileCollapse();
      App.datePicker();
      App.scrollButtons();
    },

    // Add your functions below
    logSomething: function(data) {
      console.log('This is a console test message.');
    },

    footable: function() {
      jQuery(function($){
        $('#showcase-example-1').footable({
          columns: $.get("/assets/config/columns.json"),
          rows: $.get("/assets/config/rows.json"),
          "breakpoints": {
            "xs": 480,
            "sm": 768,
            "md": 992,
            "lg": 1200,
            "xl": 1440
          },
          "paging": {
            "enabled": true,
            "countFormat": "Resultate {PF} - {PL} laden",
            "size": 25
          },
          "filtering": {
            "enabled": true
          },
          "sorting": {
            "enabled": true
          },
          "state": {
            "enabled": true
          }
        });
      });


    },


    mobileCollapse: function() {
      $(window).on("resize load", function(event){


        if ($(this).width() >= xl) {

          console.log('XL');

        } else if ($(this).width() >= lg) {

          console.log('LG');

        } else if ($(this).width() >= md) {

          console.log('MD');

        } else if ($(this).width() > sm) {

          $('.collapse-xs').addClass('in');
          console.log('SM');

        } else if ($(this).width() < sm) {

          $('.collapse-xs').removeClass('in');
          console.log('XS');

        }
      });
      $(document).ready(function(event){

        if ($(this).width() >= xl) {

          console.log('XL');

        } else if ($(this).width() >= lg) {

          console.log('LG');

        } else if ($(this).width() >= md) {

          console.log('MD');

        } else if ($(this).width() > sm) {

          $('.collapse-xs').addClass('in');
          console.log('SM');

        } else if ($(this).width() < sm) {

          $('.collapse-xs').removeClass('in');
          console.log('XS');

        }
      });
    },

    datePicker: function() {
      $('.input-group.date').datepicker({
          clearBtn: true,
          language: "de",
          orientation: "bottom auto"
      });
    },

    scrollButtons: function() {
      $(window).scroll(function() {
          // Scroll distance
          if ($(this).scrollTop() >= 50) {
              $('.fixed-side-button').fadeIn(200);
          } else {
              $('.fixed-side-button').fadeOut(200);
          }
      });
      $('#return-to-top').click(function() {
          $('body,html').animate({
              scrollTop : 0
          }, 500);
      });
      $('#search-highlight').click(function() {
          $('body,html').animate({
              scrollTop: $('.table-search').offset().top - 100
          }, 500);
          $('.table-search').focus();
      });

    }

  };
}());
